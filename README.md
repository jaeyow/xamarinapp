# README #

Continuous Delivery for Mobile Apps (Xamarin)

### What is this repository for? ###

* Demo source for the talk I did to the Sydney .NET Mobile Developers.
* Blog: https://masteringxamarin.wordpress.com/
* Meetup link: https://www.meetup.com/SydneyMobileDotNetDevelopers/events/233229471/
* Slides: https://docs.google.com/presentation/d/16rtnAfWC_2qQ59FzC1fxHVtxYqY4dqEOsvc6ViGyf3M/pub?start=false&loop=false&delayms=3000