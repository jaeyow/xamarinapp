﻿using System.Linq;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace SydXamarinApp.UITests
{
	[TestFixture(Platform.iOS)]
	public class Tests
	{
		IApp app;
		Platform platform;

		public Tests(Platform platform)
		{
			this.platform = platform;
		}

		[SetUp]
		public void BeforeEachTest()
		{
			var path = "../../../iOS/bin/Debug/iPhoneSimulator/SydXamarinApp.iOS.app";
			app = ConfigureApp.iOS.AppBundle(path)
				.DeviceIdentifier("12DFA155-8F00-4C17-8ABF-21AB70BFF67D")
				.StartApp();
		}

		[Test]
		public void WelcomeTextIsDisplayed()
		{
			AppResult[] results = app.WaitForElement(c => c.Marked("Welcome to Xamarin Forms!"));
			app.Screenshot("Welcome screen.");

			Assert.IsTrue(results.Any());
		}
	}
}

