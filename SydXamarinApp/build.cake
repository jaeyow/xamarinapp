#addin "Cake.FileHelpers"
#addin "Cake.Plist"
#addin "Cake.Xamarin"
#addin "Cake.HockeyApp"
#tool "nuget:?package=NUnit.Runners&version=2.6.4"
#addin "Cake.Slack"


var assemblyInfoFile = File("./AssemblyInfo.cs");
var plistFile = File("./iOS/Info.plist");
var projectFile = File("./iOS/SydXamarinApp.iOS.csproj");
var solutionFile = File("./SydXamarinApp.sln");

// should MSBuild treat any errors as warnings.
var treatWarningsAsErrors = "false";

// Parse release notes
var releaseNotes = ParseReleaseNotes("./RELEASENOTES.md");

// Get version
var version = releaseNotes.Version.ToString();
var epoch = (long)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;
var local = BuildSystem.IsLocalBuild;
var semVersion = string.Format("{0}.{1}", version, epoch);
var buildType = Argument<string>("build", "staging"); // staging or production

var build = Argument<string>("target", "Build");


Task("DeployOnly")
    .Does(() =>
    {
        var pathToYourPackageFile = buildType == "staging" ? "./iOS/bin/Debug/iPhone/**/*.ipa"
            : "./iOS/bin/Release/iPhone/**/*.ipa" ;

        var stagingApiToken = GetEnvironmentVariable("STAGING_API_TOKEN");
        var adhocApiToken = GetEnvironmentVariable("ADHOC_API_TOKEN");
        var stagingAppId = GetEnvironmentVariable("STAGING_APP_ID");
        var adhocAppId = GetEnvironmentVariable("ADHOC_APP_ID");

        // DeployOnly, just get the version from the plist file
        dynamic plist = DeserializePlist(plistFile);

        semVersion = plist["CFBundleVersion"];
        Information($"Semantic version: {semVersion}");

        var fileArray = GetFiles(pathToYourPackageFile);
        Information($"File path: {fileArray.First()}");

        var apiToken = buildType == "staging" ? stagingApiToken : adhocApiToken;
        var appId = buildType == "staging" ? stagingAppId : adhocAppId;

        try
        {
            UploadToHockeyApp(fileArray.First(), new HockeyAppUploadSettings
            {
                ApiToken = apiToken,
                AppId = appId,
                Version = semVersion,
                ShortVersion = semVersion,
                Status = DownloadStatus.Allowed,
                Notes = "Uploaded via continuous integration."
            },
            null);

            SendSlack($"This _is_ a `message` from *CakeBuild* :thumbsup:\r\n```SydXamarinApp v{semVersion} uploaded to HockeyApp at https://rink.hockeyapp.net/manage/dashboard```");
        }
        catch(Exception ex)
        {
            SendSlack($"This _is_ a `message` from *CakeBuild* :thumbsdown:\r\n```SydXamarinApp v{semVersion} - HockeyApp upload error```");
            throw ex;
        }
    });

private string GetEnvironmentVariable(string EnvVariableName)
{
    var envVar = EnvironmentVariable(EnvVariableName);
    if (string.IsNullOrEmpty(envVar))
    {
        throw new Exception($"The {EnvVariableName} environment variable is not defined.");
    }
    return envVar;
}

private void SendSlack(string message)
{
    var slackToken = GetEnvironmentVariable("SLACK_TOKEN");
    var slackChannel = "#general";
    var postMessageResult = Slack.Chat.PostMessage(
        token:slackToken,
        channel:slackChannel,
        text:message
    );

    if (postMessageResult.Ok)
    {
        Information("Message {0} successfully sent", postMessageResult.TimeStamp);
    }
    else
    {
        Error("Failed to send message: {0}", postMessageResult.Error);
    }
}

Task("Build")
    .IsDependentOn("RestorePackages")
    .IsDependentOn("RestoreComponents")
    .IsDependentOn("UpdateAssemblyInfo")
    .IsDependentOn("UpdateApplePlist")
    .IsDependentOn("PrepareIcons")
    .Does (() =>
{
    Information("Cleaning Build outputs first...");

    var binDir = "./SydXamarinApp/iOS/bin";
    if (DirectoryExists(binDir))
    {
        DeleteDirectory(binDir, true);
    }

    var objDir = "./SydXamarinApp/iOS/obj";
    if (DirectoryExists(objDir))
    {
        DeleteDirectory(objDir, true);
    }

      Information("Building iPhone config...");

      var config = buildType == "staging" ? "Debug" : "Release";

      DotNetBuild(projectFile, settings =>
        settings.SetConfiguration(config)
            .WithProperty("Platform", "iPhone")
            .WithProperty("OutputPath", "bin/" + config + "/iPhone/")
            .WithTarget("Build")
            .WithProperty("TreatWarningsAsErrors", treatWarningsAsErrors));

     TeamCity.PublishArtifacts(".");
});

Task("UpdateApplePlist")
    .Does (() =>
{
    dynamic plist = DeserializePlist(plistFile);
    var bundleId = buildType == "staging" ? "com.jaeyow.app.staging" : "com.jaeyow.app";

    Information($"Setting BundleId to {bundleId}");

    plist["CFBundleShortVersionString"] = version;
    plist["CFBundleVersion"] = semVersion;
    plist["CFBundleIdentifier"] = bundleId;

    SerializePlist(plistFile, plist);
});

Task("UpdateAssemblyInfo")
    .Does (() =>
{
    CreateAssemblyInfo(assemblyInfoFile, new AssemblyInfoSettings() {
        Product = "Jose Reyes",
        Version = version,
        FileVersion = version,
        InformationalVersion = semVersion,
        Copyright = "Copyright (c) Jose Reyes"
    });
});

Task("RestoreComponents")
    .Does(() =>
{

    var username = GetEnvironmentVariable("XAMARIN_USERNAME");
    var password = GetEnvironmentVariable("XAMARIN_PASSWORD");

    RestoreComponents(solutionFile, new XamarinComponentRestoreSettings()
    {
        ToolPath = "./ComponentTool/xpkg/xamarin-component.exe",
        Email = username,
        Password = password
    });
});

Task("RestorePackages")
    .Does (() =>
{
    NuGetRestore(solutionFile, new NuGetRestoreSettings
        {
            Verbosity = NuGetVerbosity.Detailed
        });
});

Task("RunUITests")
    .Does (() =>
{

    Information("Building iPhoneSimulator config...");

    DotNetBuild(projectFile, settings =>
      settings.SetConfiguration("Debug")
          .WithProperty("Platform", "iPhoneSimulator")
          .WithProperty("OutputPath", "bin/Debug/iPhoneSimulator/")
          .WithTarget("Build")
          .WithProperty("TreatWarningsAsErrors", treatWarningsAsErrors));

    Information("Building UITests...");

    var UITestProjectFile = File("./UITests/SydXamarinApp.UITests.csproj");

    DotNetBuild(UITestProjectFile, settings =>
      settings.SetConfiguration("Debug")
          .WithProperty("Platform", "Any CPU")
          .WithProperty("OutputPath", "bin/Debug/")
          .WithTarget("Build")
          .WithProperty("TreatWarningsAsErrors", treatWarningsAsErrors));


    Information("Running UITests...");

    UITest("./UITests/bin/Debug/SydXamarinApp.UITests.dll", new NUnitSettings
    {
        NoResults = false,
        ResultsFile = "UITestOutput.xml"
    });
});

Task("RunUnitTests")
    .Does (() =>
{

    Information("Building UnitTests...");

    var UnitTestProject = File("./SydXamarinApp.UnitTests/SydXamarinApp.UnitTests.csproj");
    var config = buildType == "staging" ? "Debug" : "Release";

    DotNetBuild(UnitTestProject, settings =>
      settings.SetConfiguration(config)
          .WithProperty("Platform", "Any CPU")
          .WithProperty("OutputPath", "bin/"+ config +"/")
          .WithTarget("Build")
          .WithProperty("TreatWarningsAsErrors", treatWarningsAsErrors));

    Information("Running UnitTests...");

    NUnit("./SydXamarinApp.UnitTests/bin/" + config + "/SydXamarinApp.UnitTests.dll", new NUnitSettings
    {
        NoResults = false,
        ResultsFile = "UnitTestOutput.xml"
    });
});

Task("PrepareIcons")
    .Does (() =>
{
    if (buildType == "staging")
    {
        var baseFolder = "./iOS/Resources/Images.xcassets/AppIcons.appiconset/";
        var targetFolder = "./iOS/Assets.xcassets/AppIcons.appiconset/";
        DeleteFile(targetFolder + "Icon-57.png");
        DeleteFile(targetFolder + "icon-57@2x.png");
        DeleteFile(targetFolder + "icon-60@2x.png");
        DeleteFile(targetFolder + "icon-60@3x.png");
        CopyFile(baseFolder + "Icon-57-gray.png", targetFolder + "Icon-57.png");
        CopyFile(baseFolder + "icon-57@2x-gray.png", targetFolder + "icon-57@2x.png");
        CopyFile(baseFolder + "icon-60@2x-gray.png", targetFolder + "icon-60@2x.png");
        CopyFile(baseFolder + "icon-60@3x-gray.png", targetFolder + "icon-60@3x.png");
    }
});

Task("DeliverToItunesConnect")
    .Does (() =>
{
    var pathToYourPackageFile = buildType == "staging" ? "./iOS/bin/Debug/iPhone/**/*.ipa" :
        "./iOS/bin/Release/iPhone/**/*.ipa" ;
    Information($"pathToYourPackageFile: {pathToYourPackageFile}");
    var fileArray = GetFiles(pathToYourPackageFile);
    Information($"File path: {fileArray.First()}");
    using(var process = StartAndReturnProcess("deliver",
        new ProcessSettings{
            Arguments = $"--force --ipa \"{fileArray.First()}\"",
            WorkingDirectory = ".."
         }))
    {
        process.WaitForExit();
        // This should output 0 as valid arguments supplied
        Information("Exit code: {0}", process.GetExitCode());
    }
});


RunTarget(build);
